let mapleader = " "
set backspace=2   " Backspace deletes like most programs in insert mode
set tabstop=2
set shiftwidth=2
set expandtab
set noswapfile
set history=10
set laststatus=2  " Always display the status line
set autowrite     " Automatically :write before running commands
set list listchars=tab:»·,trail:·,nbsp:·

" Numbers
set number
set numberwidth=5
syntax on

filetype plugin indent on

if filereadable(expand("~/.vimrc.bundles"))
  source ~/.vimrc.bundles
endif

if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif
