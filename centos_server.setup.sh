# Goto HOME
cd ~

# Update system
sudo yum update

# Install GIT
sudo yum install git
## Setup GIT
git config --global user.name "CentOS Server"
git config --global user.email "centosmail"

# Install ZSH
sudo yum install zsh
chsh -s /bin/zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install VIM
sudo yum install vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# Setup VIM
wget https://gitlab.com/thanhkhoait/me.config/raw/master/.vimrc
vim ~/.vimrc -c ":source %" -c :PluginInstall -c :q -c :q
export EDITOR=vim
git config --global core.editor vim

# Install rbenv
git clone git://github.com/sstephenson/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zsh_profile
echo 'eval "$(rbenv init -)"' >> ~/.zsh_profile
# Setup Ruby build
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
git clone git://github.com/jf/rbenv-gemset.git $HOME/.rbenv/plugins/rbenv-gemset
source ~/.zsh_profile

# Install Development Tools
yum groupinstall "Development Tools"
yum install -y openssl-devel readline-devel zlib-devel

# Install Ruby
rbenv install 2.1.5
rbenv rehash
rbenv global 2.1.5

# Install WGET
yum install wget

# Install Apache
sudo yum install httpd

# Install Passenger
gem install passenger

# Install EPEL
sudo yum install epel-release

# Install Nginx
sudo yum install nginx
sudo /etc/init.d/nginx start

# Install MYSQL
sudo yum install mysql-server
sudo /sbin/service mysqld start
sudo /usr/bin/mysql_secure_installation
sudo yum install mysql-devel

# Reload SHELL
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshenv
echo 'eval "$(rbenv init -)"' >> ~/.zshenv
echo 'source $HOME/.zshenv' >> ~/.zshrc
exec $SHELL

# Install ImageMagick
sudo yum install ImageMagick ImageMagick-devel

# Fix Permission denied
setenforce 0

sudo yum install httpd-devel apr-devel apr-util-devel
passenger-install-apache2-module
