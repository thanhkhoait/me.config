# Goto HOME
cd ~
sudo apt-get update

# Install GIT
echo "Install GIT"
sudo apt-get install git -y

# Install CURL
echo "Install CURL"
sudo apt-get update
sudo apt-get install curl -y

# Install VIM
echo "Install VIM"
sudo apt-get install vim -y
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# Get my config
echo "Config for VIM"
wget -O ~/.vimrc https://gitlab.com/thanhkhoait/me.config/raw/master/.vimrc
wget -O ~/.vimrc.bundles https://gitlab.com/thanhkhoait/me.config/raw/master/.vimrc.bundles
vim ~/.vimrc -c ":source %" -c :PluginInstall -c :q -c :q
wget -O ~/.vimrc.local https://gitlab.com/thanhkhoait/me.config/raw/master/.vimrc.local

# Config
echo "Config System"
export EDITOR=vim
git config --global core.editor vim
sudo dd if=/dev/zero of=/swap bs=1M count=1024
sudo mkswap /swap
sudo swapon /swap

# Install RVM & Ruby on Rails
echo "Install RVM & Ruby on Rails"
\curl -sSL https://get.rvm.io | bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://rvm.io/mpapis.asc | gpg --import
\curl -sSL https://get.rvm.io | bash -s stable --ruby
\curl -sSL https://get.rvm.io | bash -s stable --rails
source ~/.rvm/scripts/rvm
echo "source ~/.rvm/scripts/rvm" >> ~/.bashrc

# Install MySQL
echo "Install MySQL"
sudo apt-get update
sudo apt-get install mysql-server -y
sudo apt-get install libmysqlclient-dev -y
sudo service mysql restart

#Install Apache2
echo "Install Apache2"
sudo apt-get install apache2 -y
sudo service apache2 restart

#Install Passenger
echo "Install Passenger"
sudo apt-get install apache2-dev -y
gem install passenger --no-rdoc --no-ri
sudo apt-get install libcurl4-openssl-dev -y
sudo apt-get install apache2-threaded-dev -y
sudo apt-get install libapr1-dev -y
sudo apt-get install libaprutil1-dev -y
passenger-install-apache2-module

# Install NodeJS
echo "Install NodeJS"
curl --silent --location https://deb.nodesource.com/setup_4.x | sudo bash -
sudo apt-get install --yes nodejs
sudo rm -rf /usr/bin/node
sudo ln -s /usr/bin/nodejs /usr/bin/node

#Install ImageMagick
echo "Install ImageMagick"
sudo apt-get install imagemagick libmagickcore-dev -y

# Deploy config
echo "Add apps folder"
sudo mkdir /apps

echo "Finished!!!"
